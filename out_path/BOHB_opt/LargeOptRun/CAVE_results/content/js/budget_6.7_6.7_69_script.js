
  (function() {
    var fn = function() {
      Bokeh.safely(function() {
        (function(root) {
          function embed_document(root) {
            
          var docs_json = '{"30304c60-6d56-47f0-8752-f4735b614201":{"roots":{"references":[{"attributes":{"default_sort":"descending","editor":{"id":"3435","type":"StringEditor"},"field":"fANOVA","formatter":{"id":"3434","type":"StringFormatter"},"title":"fANOVA","width":100},"id":"3428","type":"TableColumn"},{"attributes":{"source":{"id":"3426","type":"ColumnDataSource"}},"id":"3431","type":"CDSView"},{"attributes":{},"id":"3436","type":"StringFormatter"},{"attributes":{"columns":[{"id":"3427","type":"TableColumn"},{"id":"3428","type":"TableColumn"},{"id":"3429","type":"TableColumn"}],"height":170,"index_position":null,"source":{"id":"3426","type":"ColumnDataSource"},"view":{"id":"3431","type":"CDSView"}},"id":"3430","type":"DataTable"},{"attributes":{},"id":"3437","type":"StringEditor"},{"attributes":{},"id":"3432","type":"StringFormatter"},{"attributes":{},"id":"3439","type":"Selection"},{"attributes":{"default_sort":"descending","editor":{"id":"3433","type":"StringEditor"},"field":"Parameters","formatter":{"id":"3432","type":"StringFormatter"},"sortable":false,"title":"Parameters","width":150},"id":"3427","type":"TableColumn"},{"attributes":{},"id":"3435","type":"StringEditor"},{"attributes":{},"id":"3438","type":"UnionRenderers"},{"attributes":{"default_sort":"descending","editor":{"id":"3437","type":"StringEditor"},"field":"LPI","formatter":{"id":"3436","type":"StringFormatter"},"title":"LPI","width":100},"id":"3429","type":"TableColumn"},{"attributes":{},"id":"3433","type":"StringEditor"},{"attributes":{},"id":"3434","type":"StringFormatter"},{"attributes":{"callback":null,"data":{"LPI":["100.00 +/- 86952.04","-","-","-","-"],"Parameters":["model_type","nn_act_f","rf_max_features","nn_hidden_dim","rf_min_samples_split"],"fANOVA":["00.24 +/- 0.93","38.12 +/- 39.32","28.92 +/- 37.45","13.09 +/- 27.50","06.57 +/- 23.31"]},"selected":{"id":"3439","type":"Selection"},"selection_policy":{"id":"3438","type":"UnionRenderers"}},"id":"3426","type":"ColumnDataSource"}],"root_ids":["3430"]},"title":"Bokeh Application","version":"1.1.0"}}';
          var render_items = [{"docid":"30304c60-6d56-47f0-8752-f4735b614201","roots":{"3430":"d40f7569-b4cc-4888-b830-632dd719ddc5"}}];
          root.Bokeh.embed.embed_items(docs_json, render_items);
        
          }
          if (root.Bokeh !== undefined) {
            embed_document(root);
          } else {
            var attempts = 0;
            var timer = setInterval(function(root) {
              if (root.Bokeh !== undefined) {
                embed_document(root);
                clearInterval(timer);
              }
              attempts++;
              if (attempts > 100) {
                console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing");
                clearInterval(timer);
              }
            }, 10, root)
          }
        })(window);
      });
    };
    if (document.readyState != "loading") fn();
    else document.addEventListener("DOMContentLoaded", fn);
  })();
