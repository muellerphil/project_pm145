
  (function() {
    var fn = function() {
      Bokeh.safely(function() {
        (function(root) {
          function embed_document(root) {
            
          var docs_json = '{"5ea9814b-9fb7-46a0-9791-3df2ccf0bac1":{"roots":{"references":[{"attributes":{"source":{"id":"3464","type":"ColumnDataSource"}},"id":"3469","type":"CDSView"},{"attributes":{"default_sort":"descending","editor":{"id":"3473","type":"StringEditor"},"field":"fANOVA","formatter":{"id":"3472","type":"StringFormatter"},"title":"fANOVA","width":100},"id":"3466","type":"TableColumn"},{"attributes":{"callback":null,"data":{"LPI":["100.00 +/- 75503.41","-","-","-"],"Parameters":["model_type","cnn_dropout","cnn_act_f","cnn_num_conv_layers"],"fANOVA":["12.22 +/- 17.62","38.26 +/- 35.08","09.29 +/- 17.76","05.41 +/- 13.01"]},"selected":{"id":"3477","type":"Selection"},"selection_policy":{"id":"3476","type":"UnionRenderers"}},"id":"3464","type":"ColumnDataSource"},{"attributes":{},"id":"3470","type":"StringFormatter"},{"attributes":{"default_sort":"descending","editor":{"id":"3471","type":"StringEditor"},"field":"Parameters","formatter":{"id":"3470","type":"StringFormatter"},"sortable":false,"title":"Parameters","width":150},"id":"3465","type":"TableColumn"},{"attributes":{},"id":"3475","type":"StringEditor"},{"attributes":{"default_sort":"descending","editor":{"id":"3475","type":"StringEditor"},"field":"LPI","formatter":{"id":"3474","type":"StringFormatter"},"title":"LPI","width":100},"id":"3467","type":"TableColumn"},{"attributes":{},"id":"3473","type":"StringEditor"},{"attributes":{"columns":[{"id":"3465","type":"TableColumn"},{"id":"3466","type":"TableColumn"},{"id":"3467","type":"TableColumn"}],"height":140,"index_position":null,"source":{"id":"3464","type":"ColumnDataSource"},"view":{"id":"3469","type":"CDSView"}},"id":"3468","type":"DataTable"},{"attributes":{},"id":"3472","type":"StringFormatter"},{"attributes":{},"id":"3474","type":"StringFormatter"},{"attributes":{},"id":"3477","type":"Selection"},{"attributes":{},"id":"3471","type":"StringEditor"},{"attributes":{},"id":"3476","type":"UnionRenderers"}],"root_ids":["3468"]},"title":"Bokeh Application","version":"1.1.0"}}';
          var render_items = [{"docid":"5ea9814b-9fb7-46a0-9791-3df2ccf0bac1","roots":{"3468":"938b9fae-f56c-4c1d-aa84-6a90b1b74bde"}}];
          root.Bokeh.embed.embed_items(docs_json, render_items);
        
          }
          if (root.Bokeh !== undefined) {
            embed_document(root);
          } else {
            var attempts = 0;
            var timer = setInterval(function(root) {
              if (root.Bokeh !== undefined) {
                embed_document(root);
                clearInterval(timer);
              }
              attempts++;
              if (attempts > 100) {
                console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing");
                clearInterval(timer);
              }
            }, 10, root)
          }
        })(window);
      });
    };
    if (document.readyState != "loading") fn();
    else document.addEventListener("DOMContentLoaded", fn);
  })();
