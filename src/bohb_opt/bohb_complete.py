import torch
import matplotlib.pyplot as plt
from torch import nn, optim
from torch.nn import functional as F, CrossEntropyLoss
from torchvision import transforms
from torch.utils.data import DataLoader
from datasets import K49
from pathlib import Path

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

from src.helper.bohb_helper import bohb_master


if __name__ == "__main__":

    no_cuda = False
    cuda = not no_cuda and torch.cuda.is_available()
    data_dir = '../data'
    print('cuda: ', cuda)
    out_path = Path('../results_cls')
    out_path.mkdir(exist_ok=True)

    load_model = False
    warm_start = False

    batch_size = 64
    seed = 1
    log_interval = 50

    torch.manual_seed(seed)
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {'num_workers': 1, 'pin_memory': True} if cuda else {}

    config = {
        'n_iterations': 20,
        'latent_space': 100,
        'min_budget': 3,
        'max_budget': 20,
        'out_path': out_path,
        'device': device,
        'data_dir': data_dir,
        'batch_size': batch_size
        }

    bohb_master(**config)
