import torch
import matplotlib.pyplot as plt
from torch import nn, optim
from torch.nn import functional as F, CrossEntropyLoss
from torchvision import transforms
from torch.utils.data import DataLoader


from datasets import K49
from pathlib import Path

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
from hpbandster.optimizers import BOHB
from hpbandster.core.worker import Worker

from ae_pytorch import AE_CNN
from vae_classifier import train, test, Classifier


def get_config_space():
    import ConfigSpace as CS
    from ConfigSpace import hyperparameters as CSH

    cs = CS.ConfigurationSpace()

    cs.add_hyperparameters([
        CSH.UniformFloatHyperparameter(name='lR',
                                       lower=1e-4, upper=1e-1, log=True),
        CSH.NormalIntegerHyperparameter(name='num_layers',
                                        mu=2, sigma=1, log=False),
        CSH.UniformIntegerHyperparameter(name='hidden_dim',
                                         lower=49, upper=300, log=False),
        CSH.CategoricalHyperparameter('act_f',
                                      choices=['relu', 'tanh', 'elu'],
                                      default_value='relu')
        ])
    return cs


class BohbWorker(Worker):

    def __init__(self, options, *args, **kwargs):

        self.device = options.get('device')
        self.latent_space = options.get('latent_space')
        self.out_path = options.get('out_path')
        self.max_budget = options.get('max_budget')
        self.ae_model = AE_CNN(self.latent_space)
        self.ae_model.load_state_dict(
                torch.load('../out_path/results_ae_100/model.pt'))
        self.ae_model.to(self.device)

        train_dataset = K49(options.get('data_dir'),
                            True, transforms.ToTensor())
        test_dataset = K49(options.get('data_dir'),
                           False, transforms.ToTensor())

        self.train_loader = DataLoader(dataset=train_dataset,
                                       batch_size=options.get('batch_size'),
                                       shuffle=True)

        self.test_loader = DataLoader(dataset=test_dataset,
                                      batch_size=options.get('batch_size'),
                                      shuffle=False)

        super(BohbWorker, self).__init__(*args, **kwargs)

    def compute(self, config, budget, **kwargs):

        logger.debug('start compute')
        classifier = Classifier(input_dim=self.latent_space,
                                hidden_dim=config['hidden_dim'],
                                output_dim=49,
                                num_layers=config['num_layers'],
                                batch_norm=True,
                                act_f=config['act_f']).to(self.device)

        optimizer = optim.Adam(classifier.parameters(), lr=1e-3)
        loss_fct = CrossEntropyLoss().to(device)

        check_dir = self.out_path / 'checkpoints'
        check_dir.mkdir(exist_ok=True, parents=True)

        config_as_str = str(config).replace(':', '_').replace('{', '').replace('\'', '')

        try:
            state = torch.load(check_dir / config_as_str)
            optimizer.load_state_dict(state['optimizer'])
            classifier.load_state_dict(state['state_dict'])
            start_epoch = state['epoch']
            logger.info('OLD CONFIG LOADED!')
        except FileNotFoundError:
            start_epoch = 0

        test_loss  = 100000000
        train_loss = 100000000
        precision = 100
        for epoch in range(start_epoch, int(budget)):

            try:

                train_loss = train(classifier=classifier,
                                   vae_model=self.ae_model,
                                   optimizer=optimizer,
                                   loss_fct=loss_fct,
                                   train_loader=self.train_loader,
                                   epoch=epoch,
                                   epochs=self.max_budget + 1,
                                   device=self.device,
                                   out_path=check_dir / config_as_str)

                test_loss, test_acc \
                    = test(classifier=classifier,
                           vae_model=self.ae_model,
                           optimizer=optimizer,
                           loss_fct=loss_fct,
                           test_loader=self.test_loader,
                           epoch=epoch,
                           epochs=self.max_budget + 1,
                           device=self.device)
                precision = 100 - test_acc

            except Exception as e:
                logger.exception(e)

        return ({'loss': float(precision),
                 'info': {'train_loss': float(train_loss),
                          'test_loss': float(test_loss),
                          'test_accuracy': float(100 - precision),
                          'config': config}
                 })


def bohb_master(latent_space, n_iterations, min_budget, max_budget, out_path, device,
                batch_size, data_dir):

    ns = hpns.NameServer(run_id='opt_fun',
                         nic_name=None,
                         working_directory=str(out_path))
    ns_host, ns_port = ns.start()

    w = BohbWorker(nameserver=ns_host,
                   nameserver_port=ns_port,
                   run_id='opt_fun',
                   options=dict(out_path=out_path,
                                device=device,
                                latent_space=latent_space,
                                batch_size=batch_size,
                                data_dir=data_dir,
                                max_budget=max_budget)
                   )
    w.run(background=True)

    result_logger = hpres.json_result_logger(directory=str(out_path),
                                             overwrite=True)

    bohb = BOHB(configspace=get_config_space(),
                run_id='opt_fun',
                host=ns_host,
                nameserver=ns_host,
                nameserver_port=ns_port,
                min_budget=min_budget,
                max_budget=max_budget,
                result_logger=result_logger
                )

    result = bohb.run(n_iterations=n_iterations)
    bohb.shutdown(shutdown_workers=True)
    ns.shutdown()

    with open(out_path / 'results.pkl', 'wb') as f:
        import pickle
        pickle.dump(result, f)

    id2config = result.get_id2config_mapping()
    incumbent = result.get_incumbent_id()
    inc_value = result.get_runs_by_id(incumbent)[-1]['loss']
    inc_cfg = id2config[incumbent]['config']

    logger.info('Inc Config:\n{}\nwith Perfomance: {:.2f}'
                .format( inc_cfg, inc_value))


if __name__ == "__main__":

    no_cuda = False
    cuda = not no_cuda and torch.cuda.is_available()
    data_dir = '../data'
    print('cuda: ', cuda)
    out_path = Path('../results_cls')
    out_path.mkdir(exist_ok=True)

    load_model = False
    warm_start = False

    batch_size = 64
    seed = 1
    log_interval = 50

    torch.manual_seed(seed)
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {'num_workers': 1, 'pin_memory': True} if cuda else {}

    config = {
        'n_iterations': 10,
        'latent_space': 100,
        'min_budget': 1,
        'max_budget': 15,
        'out_path': out_path,
        'device': device,
        'data_dir': data_dir,
        'batch_size': batch_size
        }

    bohb_master(**config)
