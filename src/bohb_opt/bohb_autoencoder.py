import torch
import matplotlib.pyplot as plt
from torch import nn, optim
from torch.nn import functional as F, MSELoss
from torchvision import transforms
from torch.utils.data import DataLoader
from src.helper.datasets import K49
from pathlib import Path

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
from hpbandster.optimizers import BOHB
from hpbandster.core.worker import Worker

from src.models.autoencoder import AE_CNN
from src.models.variational_autoencoder import train, test
from src.helper.bohb_helper import load_torch_checkpoint, bohb_master


def get_config_space():
    import ConfigSpace as CS
    from ConfigSpace import hyperparameters as CSH

    cs = CS.ConfigurationSpace()

    cs.add_hyperparameters([
        CSH.UniformFloatHyperparameter(name='lR',
                                       lower=1e-4, upper=1e-1, log=True),
        CSH.UniformFloatHyperparameter(name='weight_decay',
                                       lower=1e-5, upper=1e-1, log=True),
        CSH.UniformIntegerHyperparameter(name='latent_space',
                                         lower=50, upper=100, log=False),
        CSH.CategoricalHyperparameter('act_f',
                                      choices=['relu', 'tanh', 'elu'],
                                      default_value='relu')
        ])
    return cs


class BohbWorker(Worker):

    def __init__(self, options, *args, **kwargs):

        self.out_path = options.get('out_path')
        self.device = options.get('device')
        self.max_budget = options.get('max_budget')

        train_dataset = K49(options.get('data_dir'),
                            True, transforms.ToTensor())
        test_dataset = K49(options.get('data_dir'),
                           False, transforms.ToTensor())

        self.train_loader = DataLoader(dataset=train_dataset,
                                       batch_size=options.get('batch_size'),
                                       shuffle=True)

        self.test_loader = DataLoader(dataset=test_dataset,
                                      batch_size=options.get('batch_size'),
                                      shuffle=False)

        super(BohbWorker, self).__init__(*args, **kwargs)

    def compute(self, config, budget, **kwargs):

        logger.debug('start compute')

        model = AE_CNN(latent_space_size=config.get('latent_space'),
                       act_f=config.get('act_f')).to(self.device)

        weight_decay = config.get('weight_decay')
        weight_decay = 0 if weight_decay < 1e-4 else weight_decay

        optimizer = optim.Adam(model.parameters(),
                               lr=config.get('lR'),
                               weight_decay=weight_decay)

        loss_fct = MSELoss().to(device)

        check_dir = self.out_path / 'checkpoints'
        check_dir.mkdir(exist_ok=True, parents=True)

        config_as_str = \
            str(config).replace(':', '_').replace('\n', '') \
                       .replace('{', '').replace('\'', '').replace('}', '') \
                       .replace(' ', '')

        check_dir = check_dir / config_as_str

        start_epoch = load_torch_checkpoint(check_dir=check_dir,
                                            optimizer=optimizer,
                                            model=model)
        val_loss  = 100000000
        train_loss = 100000000
        for epoch in range(start_epoch, int(budget)):
            try:
                train_loss = train(model=model,
                                   optimizer=optimizer,
                                   epoch=epoch+1,
                                   train_loader=self.train_loader,
                                   loss_mse=loss_fct,
                                   epochs=int(budget),
                                   out_path=check_dir,
                                   device=self.device
                                   )

                val_loss = test(model=model,
                                epoch=epoch+1,
                                test_loader=self.test_loader,
                                loss_mse=loss_fct,
                                epochs=int(budget),
                                batch_size=self.test_loader.batch_size,
                                out_path=self.out_path,
                                device=self.device)

            except Exception as e:
                logger.exception(e)

        return ({'loss': float(val_loss),
                 'info': {'train_loss': float(train_loss),
                          'test_loss': float(val_loss),
                          'config': config}
                 })


if __name__ == "__main__":

    no_cuda = False
    cuda = not no_cuda and torch.cuda.is_available()
    data_dir = './data'
    print('cuda: ', cuda)

    out_path = Path('./out_path/BOHB_opt/AutoEncoder')
    out_path.mkdir(exist_ok=True, parents=True)
    (out_path / 'images').mkdir(exist_ok=True)

    load_model = False
    warm_start = False

    batch_size = 64
    seed = 1
    log_interval = 50

    torch.manual_seed(seed)
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {'num_workers': 1, 'pin_memory': True} if cuda else {}

    config = {
        'worker': BohbWorker,
        'n_iterations': 10,
        'latent_space': None,
        'min_budget': 2,
        'max_budget': 20,
        'out_path': out_path,
        'device': device,
        'data_dir': data_dir,
        'batch_size': batch_size,
        'config_space': get_config_space(),
        }

    bohb_master(**config)
