import torch
import matplotlib.pyplot as plt
from torch import nn, optim
from torch.nn import functional as F, MSELoss, CrossEntropyLoss
from torchvision import transforms
from torch.utils.data import DataLoader
from src.helper.datasets import K49Encoded, K49
from pathlib import Path

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

from hpbandster.core.worker import Worker
from src.helper.bohb_helper import parse_args
from src.darts.model_search import Network as DartsEncoder
from src.models.nn import train, test
from src.helper.bohb_helper import load_torch_checkpoint, bohb_master
from src.models.nn import Classifier


def get_config_space():
    import ConfigSpace as CS
    from ConfigSpace import hyperparameters as CSH

    cs = CS.ConfigurationSpace()

    cs.add_hyperparameters([
        CSH.UniformFloatHyperparameter(name='lR',
                                       lower=1e-4, upper=1e-1, log=True),
        CSH.NormalIntegerHyperparameter(name='num_layers',
                                        mu=2, sigma=1, log=False),
        CSH.UniformIntegerHyperparameter(name='hidden_dim',
                                         lower=49, upper=300, log=False),
        CSH.CategoricalHyperparameter('act_f',
                                      choices=['relu', 'tanh', 'elu'],
                                      default_value='relu'),
        CSH.UniformFloatHyperparameter(name='weight_decay',
                                       lower=1e-5, upper=1e-1, log=True)
        ])
    return cs


class BohbWorker(Worker):

    def __init__(self, options, *args, **kwargs):

        self.out_path = options.get('out_path')
        self.device = options.get('device')
        self.max_budget = options.get('max_budget')
        self.latent_space = options.get('latent_space')

        # Load Encoder Model
        darts_encoder = DartsEncoder(device=device, nodes=4)
        state = torch.load(
            './out_path/TrainedNets/Darts_4_nodes/state_fixed_arch.pt',
            map_location=device)
        darts_encoder.load_state_dict(state['state_dict'])
        old_alphas = state['alphas']
        darts_encoder.arch_parameters = old_alphas
        darts_encoder.alphas_normal = old_alphas[0]
        darts_encoder.alphas_reduce = old_alphas[1]
        darts_encoder.alphas_upsample = old_alphas[2]
        darts_encoder.to(device)
        self.ae_model = darts_encoder

        # train_dataset = K49Encoded(options.get('data_dir'),
        #                            True, transforms.ToTensor())
        # test_dataset = K49Encoded(options.get('data_dir'),
        #                           False, transforms.ToTensor())

        train_dataset = K49(options.get('data_dir'),
                            True, transforms.ToTensor())
        test_dataset = K49(options.get('data_dir'),
                           False, transforms.ToTensor())

        self.train_loader = DataLoader(dataset=train_dataset,
                                       batch_size=options.get('batch_size'),
                                       shuffle=True)

        self.test_loader = DataLoader(dataset=test_dataset,
                                      batch_size=options.get('batch_size'),
                                      shuffle=False)

        super(BohbWorker, self).__init__(*args, **kwargs)

    def compute(self, config, budget, **kwargs):

        logger.debug('start compute')
        config['num_layers'] = 1 if config['num_layers'] < 1 else config['num_layers']
        classifier = Classifier(input_dim=self.latent_space,
                                hidden_dim=config['hidden_dim'],
                                output_dim=49,
                                num_layers=config['num_layers'],
                                batch_norm=True,
                                act_f=config['act_f']).to(self.device)

        weight_decay = config.get('weight_decay')
        weight_decay = 0 if weight_decay < 1e-4 else weight_decay

        optimizer = optim.Adam(classifier.parameters(),
                               lr=config.get('lR'),
                               weight_decay=weight_decay)

        loss_fct = CrossEntropyLoss().to(device)

        check_dir = self.out_path / 'checkpoints'
        check_dir.mkdir(exist_ok=True, parents=True)

        config_as_str = \
            str(config).replace(':', '_').replace('\n', '') \
                       .replace('{', '').replace('\'', '').replace('}', '') \
                       .replace(' ', '').replace(',', '')

        check_dir = check_dir / config_as_str

        start_epoch = load_torch_checkpoint(check_dir=check_dir,
                                            optimizer=optimizer,
                                            model=classifier)
        val_loss   = 100000000
        train_loss = 100000000
        precision  = 100

        for epoch in range(start_epoch, int(budget)):
            try:
                train_loss, train_acc = train(classifier=classifier,
                                   vae_model=self.ae_model,
                                   optimizer=optimizer,
                                   loss_fct=loss_fct,
                                   train_loader=self.train_loader,
                                   epoch=epoch+1,
                                   epochs=int(budget),
                                   device=self.device,
                                   out_path=check_dir)
                """
                def test(classifier, vae_model, loss_fct,
                     test_loader, epoch, epochs, device, debug=True):
            
                """
                test_loss, test_acc \
                    = test(classifier=classifier,
                           vae_model=self.ae_model,
                           loss_fct=loss_fct,
                           test_loader=self.test_loader,
                           epoch=epoch+1,
                           epochs=int(budget),
                           device=self.device,
                           debug=True)
                precision = 100 - test_acc

            except Exception as e:
                logger.exception(e)

        return ({'loss': float(precision),
                 'info': {'train_loss': float(train_loss),
                          'test_loss': float(val_loss),
                          'test_accuracy': float(100 - precision),
                          'config': config}
                 })


if __name__ == "__main__":


    args = parse_args()

    no_cuda = (args.no_cuda.lower() == 'true')
    cuda = not no_cuda and torch.cuda.is_available()
    data_dir = args.data_path
    print('cuda: ', cuda)

    # out_path = Path('./out_path/BOHB_opt/AutoEncoderClassifier')
    out_path = Path(args.out_path)
    out_path.mkdir(exist_ok=True, parents=True)
    (out_path / 'images').mkdir(exist_ok=True)

    load_model = False
    warm_start = False

    batch_size = 64
    seed = 1
    log_interval = 50

    torch.manual_seed(seed)
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {'num_workers': 1, 'pin_memory': True} if cuda else {}

    config = {
        'worker': BohbWorker,
        'n_iterations': 10,
        'latent_space': 100,
        'min_budget': 2,
        'max_budget': 20,
        'out_path': out_path,
        'device': device,
        'data_dir': data_dir,
        'batch_size': batch_size,
        'config_space': get_config_space(),
        'run_id': args.run_id,
        'nic_name': args.nic_name,
        'array_id': args.array_id,
        'opts': {'act_f': 'tanh'}
        }

    bohb_master(**config)
