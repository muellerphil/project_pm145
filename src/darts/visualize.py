import sys
from src.darts.model_search import Genotype
from graphviz import Digraph


def plot(genotype, filename):
  g = Digraph(
      format='pdf',
      edge_attr=dict(fontsize='20', fontname="times"),
      node_attr=dict(style='filled', shape='rect', align='center',
                     fontsize='20', height='0.5', width='0.5', penwidth='2',
                     fontname="times"),
      engine='dot')
  g.body.extend(['rankdir=LR'])

  number_nodes = (len(genotype) // 2) + 1
  edges_from_array = [[i for i in range(node + 1)]
                       for node in range(number_nodes)]

  # Create Nodes
  # g.node("c_{k-1}", fillcolor='darkseagreen2')
  g.node("c_{k}", fillcolor='palegoldenrod')

  for i in range(1, number_nodes+1):
      g.node(str(i), fillcolor='lightblue')

      # Create Edges from nodes to sink
      g.edge(str(i), "c_{k}", fillcolor='gray')

  # Create Edges
  start = 0
  for to_node, edges_from in enumerate(edges_from_array):
    to_node = str(to_node + 1)

    if len(edges_from) == 1:
        g.edge('c_{k_1}', str(1), label=genotype[0][0], fillcolor='gray')
        start += 1
        continue

    for i in range(2):
        incoming_edge = genotype[start + i][1]
        from_node = edges_from[incoming_edge]
        from_node = 'c_{k_1}' if from_node == 0 else str(from_node)
        g.edge(from_node, to_node, genotype[start + i][0], fillcolor='gray')
    start += 2

  g.render(filename, view=True)


if __name__ == '__main__':
  #genotype_name = sys.argv[1]
  with open('./out_path/DARTS/architecture', 'r') as f:
      genotype = f.readline()
      print('Finished reading. Genotype is {}'.format(genotype))
  try:
    genotype = eval(genotype)
  except AttributeError:
    print("{} is not specified in genotypes.py".format(genotype_name)) 
    sys.exit(1)

  print(genotype.normal)
  print(genotype.reduce)
  print(genotype.upsample)
  
  print('Start Plotting')
  plot(genotype.normal, "normal")
  plot(genotype.reduce, "reduction")
  plot(genotype.upsample, 'upsample')
  print('')
