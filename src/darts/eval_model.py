import os
import sys
import numpy as np
import torch
import src.darts.utils as utils
import logging
import argparse
import torch.nn as nn
import torch.nn.functional as F
from tqdm import tqdm as tqdm
from torchvision import transforms
from src.helper.datasets import K49
from torch.utils.data import DataLoader

from .model_search import Network
from pathlib import Path
from torchvision.utils import save_image


class Architect(object):
    """
    Base class for the architect, which is just an optimizer (different from
    the one used to update the model parameters) for the architectural parameters.
    """
    def __init__(self, model):
        """
        :model: nn.Module; search model
        """
        self.model = model
        self.criterion = nn.MSELoss(reduction='mean')
        self.arch_optimizer = torch.optim.Adam(self.model.arch_parameters,
                                               lr=3e-4,
                                               betas=(0.5, 0.999),
                                               weight_decay=1e-3)

    def step(self, input_valid, target_valid):
        """
        This method computes a gradient step in the architecture space, i.e.
        updates the self.model.alphas_normal and self.model.alphas_reduce by
        the gradient of the validation loss with respect to these alpha
        parameters.
        :input_valid: torch.Tensor; validation mini-batch
        :target_valid: torch.Tensor: ground truth labels of this mini-batch
        """
        self.arch_optimizer.zero_grad()
        # do a forward pass using the validation mini-batch input
        prediction = self.model(input_valid)
        # compute the loss using self.criterion and backpropagate to
        # compute the gradients w.r.t. the alphas
        loss = self.criterion(prediction, target_valid)
        # do a step in the architecture space using the
        # self.arch_optimizer
        loss.backward()
        self.arch_optimizer.step()


def train_only_net(train_loader, model, criterion, optimizer, device):
    """
    Training loop. This function computes the DARTS loop, i.e. it takes one step
    in the weight space.
    """
    objs = utils.AvgrageMeter()
    model.train()

    t = tqdm(train_loader)

    for step, (input, target) in enumerate(t):
        input, target = input.to(device), target.to(device)

        # update the search model parameters with the updated architecture
        # NOTE: The architecture is kept fixed here, just the search model
        # weights/parameters are updated
        optimizer.zero_grad()
        logits = model(input, eval=True)
        loss = criterion(logits, input)
        loss.backward()
        optimizer.step()

        objs.update(loss.item(), input.size(0))

        t.set_description('(==> TRAIN) mini-batch {:3d}|{},'
                          ' loss : {:.4f} '
                          .format(step, len(train_loader), objs.avg))
    return objs.avg


def infer(valid_loader, model, criterion, device, out_path, epoch):
    """
    Compute the accuracy on the validation set (the same used for updating the
    architecture).
    """
    objs = utils.AvgrageMeter()
    accr = utils.AvgrageMeter()
    model.eval()

    t = tqdm(valid_loader)

    with torch.no_grad():
        for step, (input, target) in enumerate(t):
            input, target = input.to(device), target.to(device)
            target = input

            logits = model(input, eval=True)
            loss = criterion(logits, target)

            # _accr= utils.accuracy(logits, target)
            objs.update(loss.item(), input.size(0))
            # accr.update(_accr.item(), input.size(0))

            # logging.info('valid mini-batch %03d, loss=%e accuracy=%f', step,
            #              objs.avg, accr.avg)
            t.set_description('(==> VALID) mini-batch {:3d}|{}, loss : {:.4f} '
                              .format(step, len(valid_loader), objs.avg))

            if step == 0:
                n = min(input.size(0), 8)
                comparison = \
                    torch.cat([input[:n],
                               logits.view(input.size(0), 1, 28, 28)[:n]])

                save_image(comparison.cpu(),
                           out_path / ('reconstruction_'
                           + str(epoch) + '.png'), nrow=n)


    return objs.avg


def main(args):
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    logging.info("args = %s", args)

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    logging.info(f'Device: {device}')
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}

    data_dir = '../data'
    data_augmentations = transforms.ToTensor()

    train_dataset = K49(data_dir, True, data_augmentations)
    test_dataset = K49(data_dir, False, data_augmentations)

    batch_size = 32
    train_loader = DataLoader(dataset=train_dataset,
                              batch_size=batch_size,
                              shuffle=True)

    valid_loader = DataLoader(dataset=test_dataset,
                              batch_size=batch_size,
                              shuffle=False)

    criterion = nn.MSELoss(reduction='mean').to(device)
    model = Network(device, nodes=4).to(device)

    logging.info("param size = %fMB",
            np.sum(np.prod(v.size()) for name, v in model.named_parameters())/1e6)

    optimizer = torch.optim.SGD(
            model.parameters(),
            args.learning_rate,
            momentum=0.9,
            weight_decay=args.weight_decay)

    out_path = Path('../out_path/DARTS/')
    image_path = out_path / 'images'
    image_path.mkdir(exist_ok=True, parents=True)

    old_state = torch.load(out_path / 'state.pt')
    old_alphas = old_state['alphas']
    model.arch_parameters = old_alphas
    model.alphas_normal = old_alphas[0]
    model.alphas_reduce = old_alphas[1]
    model.alphas_upsample = old_alphas[2]

    for epoch in range(args.epochs):
        logging.info("Starting epoch %d/%d", epoch+1, args.epochs)

        train_acc = train_only_net(train_loader, model, criterion,
                                   optimizer, device)
        logging.info('train_acc %f', train_acc)

        state = {
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'optimizer': optimizer.state_dict(),
            'alphas': model.arch_parameters
            }
        torch.save(state, out_path / 'state_fixed_arch.pt')

        # validation
        valid_acc = infer(valid_loader, model, criterion, device, image_path, epoch+1)
        logging.info('valid_acc %f', valid_acc)

        # compute the discrete architecture from the current alphas
        genotype = model.genotype()
        logging.info('genotype = %s', genotype)

        print(F.softmax(model.alphas_normal, dim=-1))
        print(F.softmax(model.alphas_reduce, dim=-1))
        print(F.softmax(model.alphas_upsample, dim=-1))
        with open(out_path / 'architecture', 'w') as f:
            f.write(str(genotype))


if __name__ == '__main__':
    parser = argparse.ArgumentParser("mnist")
    parser.add_argument('--data', type=str, default='./data', help='location of the data corpus')
    parser.add_argument('--batch_size', type=int, default=64, help='batch size')
    parser.add_argument('--learning_rate', type=float, default=0.025, help='init learning rate')
    parser.add_argument('--weight_decay', type=float, default=3e-4, help='weight decay')
    parser.add_argument('--epochs', type=int, default=5, help='num of training epochs')
    parser.add_argument('--save', type=str, default='logs', help='path to logs')
    parser.add_argument('--seed', type=int, default=2, help='random seed')
    args = parser.parse_args()

    # logging utilities
    os.makedirs(args.save, exist_ok=True)
    log_format = '%(asctime)s %(message)s'
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
            format=log_format, datefmt='%m/%d %I:%M:%S %p')
    fh = logging.FileHandler(os.path.join(args.save, 'log.txt'))
    fh.setFormatter(logging.Formatter(log_format))
    logging.getLogger().addHandler(fh)

    main(args)

