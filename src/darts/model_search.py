import torch
import torch.nn as nn
import torch.nn.functional as F
from src.darts.utils import ReLUConvBN, Conv, Identity, FactorizedReduce,\
    ReLUConvTBN
from torch.autograd import Variable
from collections import namedtuple

# this object will be needed to represent the discrete architecture extracted
# from the architectural parameters. See the method genotype() below.
Genotype = namedtuple('Genotype',
                      'normal normal_concat '
                      'reduce reduce_concat'
                      ' upsample upsample_concat')

# operations set
OPS = {
    'avg_pool_3x3' : lambda C, stride: nn.AvgPool2d(3,
                                                    stride=stride,
                                                    padding=1,
                                                    count_include_pad=False),
    'max_pool_3x3' : lambda C, stride: nn.MaxPool2d(3, stride=stride, padding=1),
    'skip_connect' : lambda C, stride: Identity() if stride == 1 else FactorizedReduce(C, C),
    'conv_3x3'     : lambda C, stride: Conv(C, C, 3, stride, 1),
}


PRIMITIVES = list(OPS.keys()) # operations set as list of strings


class MixedOp(nn.Module):
    """Base class for the mixed operation."""
    def __init__(self, C, stride):
        """
        :C: int; number of filters in each convolutional operation
        :stride: int; stride of the convolutional/pooling kernel
        """
        super(MixedOp, self).__init__()
        self._ops = nn.ModuleList()
        # iterate thtough the operation set and append them to self._ops
        for primitive in PRIMITIVES:
            op = OPS[primitive](C, stride)
            self._ops.append(op)

    def forward(self, x, alphas, eval=False):
        """
        Compute the softmax of alphas and multiply that element-wise with the
        corresponding output of the operations.

        :x: torch.Tensor; input tensor
        :alphas: torch.Tensor; architectural parameters, either alphas_normal
        or alphas_reduce
        """
        #TODO: compute the softmax of the alphas parameter
        alphas_softmax = F.softmax(alphas, dim=-1)

        if eval:
            alphas_softmax = alphas == alphas.max(dim=0)[0]

        list_of_tensors = []
        for i, op in enumerate(self._ops):
            out = op(x)
            out = out * alphas_softmax[i]
            list_of_tensors.append(out)

        result = torch.zeros(size=out.shape, device=x.device)
        for i in range(len(list_of_tensors)):
            result = result + list_of_tensors[i]

        return result



class Cell(nn.Module):
    """Base class for the cells in the search model."""
    def __init__(self, nodes, C_prev, C, type):
        """
        :nodes: int; number of intermediate nodes in the cell
        :C_prev: int; number of feature maps incoming to the cell
        :C: int; number of filters in each convolutional operation
        :type; str ['up', 'down', 'normal']
        """
        super(Cell, self).__init__()
        self.reduction = type.lower() == 'down'
        self.type = type.lower()
        # this preprocessing operation is added to keep the dimensions of the
        # tensors going to the intermediate nodes the same.
        self.preprocess = ReLUConvBN(C_prev, C, 1, 1, 0) \
            if type.lower() in ['down', 'normal'] \
            else ReLUConvTBN(C_in=C_prev, C_out=C, kernel_size=3, stride=2, padding=1)
        self._nodes = nodes

        self._ops = nn.ModuleList()
        # iterate throughout each edge of the cell and create a MixedOp
        for i in range(self._nodes):
            for j in range(1+i):
                stride = 2 if self.reduction and j < 1 else 1
                op = MixedOp(C, stride)
                self._ops.append(op)

    def forward(self, input, alphas, eval=False):
        preprocessed_input = self.preprocess(input)
        alphas = alphas[1] if self.type == 'down'\
            else (alphas[0] if self.type == 'normal' else alphas[2])

        # which ops should we use
        def get_edges(alphas):
            edges_to_use = []
            gene = []
            n = 1
            start = 0
            for i in range(self._nodes):
                end = start + n
                W = alphas[start:end].clone()

                edges = sorted(range(i + 1),
                               key=lambda x: -max(W[x][k] for k in range(len(W[x]))))[:2]
                edges = [e + start for e in edges]
                edges_to_use += edges

                start = end
                n += 1
            edges_to_use.sort()
            return edges_to_use

        edges = get_edges(alphas) if eval else list(range(len(alphas)))

        states = [preprocessed_input]
        offset = 0
        op_num = 0
        for i in range(self._nodes):
            s = None
            for j, h in enumerate(states):
                if op_num in edges:
                    intermediate = self._ops[offset+j](h, alphas[offset+j], eval=eval)
                    s = intermediate if s is None else sum([s, intermediate])
                op_num += 1
            offset += len(states)
            states.append(s)

            # s = sum(self._ops[offset+j](h, alphas[offset+j], eval=eval)
            #         for j, h in enumerate(states))

            # offset += len(states)
            # states.append(s)

        # concatenate the outputs of only the intermediate nodes to form the
        # output node.
        out = torch.cat(states[-self._nodes:], dim=1)
        return out


class Network(nn.Module):
    """Base class for the search model (one-shot model)."""
    def __init__(self, device, nodes=2):
        """
        :device: str; 'cuda' or 'cpu'
        :nodes: int; number of intermediate nodes in each cell
        """
        super(Network, self).__init__()
        self._nodes = nodes

        # the one-shot model we are going to use is composed of one reduction
        # cell followed by one normal cell and another reduction cell. The
        # architecture of the 2 reduction cells is the same (they share the
        # alpha_reduction parameter). However the weights of the corresponding
        # operations (convolutional filters) is different.
        reduction_cell_1 = Cell(nodes, 1, 16, type='down')

        normal_cell_1 = Cell(nodes, nodes*16, 16, type='normal')
        reduction_cell_2 = Cell(nodes, nodes*16, 32, type='down')

        self.encoder_cells = nn.ModuleList([reduction_cell_1,
                                            normal_cell_1,
                                            reduction_cell_2])

        # Latent Space
        self.fc1 = nn.Linear(nodes*32*7*7, 100)
        self.fc_bn1 = nn.BatchNorm1d(100)

        self.fc2 = nn.Linear(100, nodes*32*7*7)
        self.fc_bn2 = nn.BatchNorm1d(nodes*32*7*7)

        # Decoder
        upsampling_cell_1 = Cell(nodes, nodes*32, 16, type='up')
        normal_cell_2 = Cell(nodes, nodes*16, 16, type='normal')
        # TODO: Last cell returs 64 x nodes x 28 x 28
        #  --> how to bring down nodes to 1
        upsampling_cell_2 = Cell(nodes, nodes*16, 1, type='up')

        self.decoder_cells = nn.ModuleList([upsampling_cell_1,
                                            normal_cell_2,
                                            upsampling_cell_2])

        self.flatten_conv = nn.Conv2d(in_channels=nodes, out_channels=1,
                                      kernel_size=1, stride=1, padding=0)

        #self.global_pooling = nn.AdaptiveAvgPool2d(1)
        #self.classifier = nn.Linear(nodes*32, 10)

        # initialize the architectural parameters to be equal. We also add a
        # tiny randomly sampled noise for numerical stability.
        self._initialize_alphas(device)

    def forward(self, input, eval=False):
        encoded = self.encode(input, eval)
        decoded = self.decode(encoded, eval)
        return decoded

        # out = self.global_pooling(x)
        # logits = self.classifier(out.view(out.size(0),-1))
        # return logits

    def encode(self, input, eval=False):
        x = input
        for i, cell in enumerate(self.encoder_cells):
            x = cell(x, self.arch_parameters, eval)

        encoded = F.relu(self.fc_bn1(self.fc1(x.view(x.size(0), -1))))
        return encoded

    def decode(self, input, eval=False):

        input = F.relu(self.fc_bn2(self.fc2(input)))
        # recreate image like shape
        x = input.view(input.size(0), self._nodes*32, 7, 7)
        for i, cell in enumerate(self.decoder_cells):
            x = cell(x, self.arch_parameters, eval)

        # TODO: Think about this step!!
        x = self.flatten_conv(x)
        x = torch.sigmoid(x)
        return x

    def _initialize_alphas(self, device):
        """
        Initialize the architectural parameters for the normal and reduction
        cells. The dimensions of each of these variables will be k x num_ops,
        where k is the number of edges in the cell and num_ops is the
        operation set size.
        """
        k = sum(1 for i in range(self._nodes) for n in range(1+i))
        num_ops = len(PRIMITIVES)

        self.alphas_normal = Variable(1e-3*torch.randn(k, num_ops, device=device),
                                      requires_grad=True)
        self.alphas_reduce = Variable(1e-3*torch.randn(k, num_ops, device=device),
                                      requires_grad=True)
        self.alphas_upsample = Variable(1e-3*torch.randn(k, num_ops, device=device),
                                        requires_grad=True)
        self.arch_parameters = [
            self.alphas_normal,
            self.alphas_reduce,
            self.alphas_upsample
        ]

    def genotype(self):
        """
        Method for getting the discrete architecture, represented as a Genotype
        object from the DARTS search model.
        """

        def _parse(alphas):
            gene = []
            n = 1
            start = 0
            for i in range(self._nodes):
                end = start + n
                W = alphas[start:end].copy()
                edges = sorted(range(i + 1),
                               key=lambda x: -max(W[x][k] for k in range(len(W[x]))))[:2]

                for j in edges:
                    k_best = None
                    for k in range(len(W[j])):
                        if k_best is None or W[j][k] > W[j][k_best]:
                            k_best = k
                    gene.append((PRIMITIVES[k_best], j))
                start = end
                n += 1
            return gene

        gene_normal = _parse(F.softmax(self.alphas_normal, dim=-1).data.cpu().numpy())
        gene_reduce = _parse(F.softmax(self.alphas_reduce, dim=-1).data.cpu().numpy())
        gene_upsample = _parse(F.softmax(self.alphas_upsample, dim=-1).data.cpu().numpy())

        concat = range(1, self._nodes+1)
        genotype = Genotype(
            normal=gene_normal, normal_concat=concat,
            reduce=gene_reduce, reduce_concat=concat,
            upsample=gene_upsample, upsample_concat=concat,
        )

        return genotype

