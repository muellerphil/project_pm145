import torch
import matplotlib.pyplot as plt
from tqdm import tqdm as tqdm
from skimage import io, transform
from torch import nn, optim
from torch.nn import functional as F, CrossEntropyLoss
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision.utils import save_image
from datasets import K49
from helpermodels.variational_autoencoder import VAE_CNN
from helpermodels.autoencoder import AE_CNN
from models.nn import Classifier, train, test
from utils import AvgrageMeter, to_one_hot
from pathlib import Path
from utils import accuracy

if __name__ == "__main__":

    no_cuda = False
    cuda = not no_cuda and torch.cuda.is_available()
    data_dir = '../data'
    out_path = Path('../results_cls')
    out_path.mkdir(exist_ok=True)

    load_model = False
    warm_start = False

    batch_size = 64
    epochs = 50
    seed = 1
    log_interval = 50

    torch.manual_seed(seed)
    data_augmentations = transforms.ToTensor()
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {'num_workers': 1, 'pin_memory': True} if cuda else {}

    latent_space = 100
    vae_model = AE_CNN(latent_space).to(device)
    vae_model.load_state_dict(torch.load('../out_path/results_ae_{}/model.pt'
                                         .format(latent_space),
                                         map_location=device))

    try:
        import pickle
        result = pickle.load((out_path / 'results.pkl').open('rb'))
        id2config = result.get_id2config_mapping()
        incumbent = result.get_incumbent_id()
        inc_value = result.get_runs_by_id(incumbent)[-1]['loss']
        config = id2config[incumbent]['config']
        lr = config.get('nn_lR')
        print('Inc Config:\n{}\nwith Perfomance: {:.2f}'
              .format(config, inc_value))
    except:
        config = dict(nn_act_f='relu',
                      nn_hidden_dim=250,
                      nn_num_layers=4)
        lr = 0.0001

    classifier = Classifier(input_dim=latent_space,
                            hidden_dim=config.get('nn_hidden_dim'),
                            output_dim=49,
                            num_layers=config.get('nn_num_layers'),
                            batch_norm=True,
                            act_f=config.get('nn_act_f')).to(device)

    print('Classifier: ', sum([p.numel() for p in classifier.parameters()]))
    print('AutoEnc: ', sum([p.numel() for p in vae_model.parameters()]))

    optimizer = optim.Adam(classifier.parameters(), lr=lr)
    loss_fct = CrossEntropyLoss().to(device)

    train_dataset = K49(data_dir, True, data_augmentations)
    test_dataset = K49(data_dir, False, data_augmentations)

    # Make data batch iterable
    # Could modify the sampler to not uniformly random sample
    train_loader = DataLoader(dataset=train_dataset,
                              batch_size=batch_size,
                              shuffle=True)

    test_loader = DataLoader(dataset=test_dataset,
                             batch_size=batch_size,
                             shuffle=False)


    train_losses = []
    test_losses = []
    for epoch in range(1, epochs + 1):

        train_loss = train(classifier=classifier,
                           vae_model=vae_model,
                           optimizer=optimizer,
                           loss_fct=loss_fct,
                           train_loader=train_loader,
                           epoch=epoch,
                           epochs=epochs+1,
                           device=device,
                           out_path=out_path / 'model.pt')

        test_loss, test_accuracy = test(classifier=classifier,
                                        vae_model=vae_model,
                                        loss_fct=loss_fct,
                                        test_loader=test_loader,
                                        epoch=epoch,
                                        epochs=epochs+1,
                                        device=device)

        train_losses.append(train_loss)
        test_losses.append(test_loss)

    plt.figure(figsize=(15, 10))
    plt.plot(range(len(train_losses)), train_losses)
    plt.plot(range(len(test_losses)), test_losses)
    plt.title("Validation loss and loss per epoch", fontsize=18)
    plt.xlabel("epoch", fontsize=18)
    plt.ylabel("loss", fontsize=18)
    plt.legend(['Training Loss', 'Validation Loss'], fontsize=14)
    plt.savefig(out_path / 'lc_hist.png')
