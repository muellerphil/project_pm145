import torch
import os
import logging
import numpy as np
from torch.autograd import Variable


class AvgrageMeter(object):

    def __init__(self):
        self.reset()

    def reset(self):
        self.avg = 0
        self.sum = 0
        self.cnt = 0

    def update(self, val, n=1):
        self.sum += val * n
        self.cnt += n
        self.avg = self.sum / self.cnt


def accuracy(output, target, topk=(1,)):
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0)
        res.append(correct_k.mul_(100.0/batch_size))
    return res


def load_checkpoint(model, optimizer, filename):
    start_epoch = 0

    if os.path.isfile(filename):
        # print("=> loading checkpoint '{}'".format(filename))
        checkpoint = torch.load(filename)
        start_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        logging.info("=> loaded checkpoint '{}' (epoch {})"
              .format(filename, checkpoint['epoch']))
    else:
        print("=> no checkpoint found at '{}'".format(filename))

    return model, optimizer, start_epoch


def to_one_hot(y, n_dims=None):
    """
    Take integer y (tensor or variable) with n dims and
    convert it to 1-hot representation with n+1 dims.
    """
    y_tensor = y.data if isinstance(y, Variable) else y
    y_tensor = y_tensor.type(torch.LongTensor).view(-1, 1)
    n_dims = n_dims if n_dims is not None else int(torch.max(y_tensor)) + 1
    y_one_hot = torch.zeros(y_tensor.size()[0], n_dims).scatter_(1, y_tensor, 1)
    y_one_hot = y_one_hot.view(*y.shape, -1)

    return Variable(y_one_hot) if isinstance(y, Variable) else y_one_hot


def create_encodings(loader, ae_model, latent_space, device, train_data):
    logger = logging.getLogger('Utils')

    logger.info('Calculate Feature Map')
    unsorted_labels = loader.dataset.labels
    feature_map = np.zeros([unsorted_labels.shape[0], latent_space])
    sorted_labels = np.zeros(unsorted_labels.shape)

    for batch_idx, (images, labels_batch) in enumerate(loader):
        images = images.to(device)

        with torch.no_grad():
            ae_model.eval()
            feat_map = ae_model.encode(images)
            if type(feat_map) is tuple:
                feat_map, _ = feat_map

            start_idx = batch_idx * loader.batch_size
            end_idx = min(len(loader.dataset),
                          start_idx + loader.batch_size)
            feature_map[start_idx:end_idx] = feat_map.reshape(
                    (-1, latent_space)).cpu().numpy()
            sorted_labels[start_idx:end_idx] = labels_batch.cpu().numpy()

    logger.info('save')
    if train_data:
        np.save('../data/train_feat.npy', feature_map)
        np.save('../data/train_labels.npy', sorted_labels)
    else:
        np.save('../data/test_feat.npy', feature_map)
        np.save('../data/test_labels.npy', sorted_labels)
    logger.info('save_finish')
