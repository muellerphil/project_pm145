import torch
import argparse
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

import ConfigSpace as CS
import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
from hpbandster.optimizers import BOHB
from hpbandster.core.worker import Worker

from torch import nn, optim
from torch.nn import functional as F, CrossEntropyLoss
from torchvision import transforms
from torch.utils.data import DataLoader
from src.helper.datasets import K49

from ConfigSpace import hyperparameters as CSH, forbidden as CSF,\
    conditions as CSC

from src.models.nn import Classifier, train, test
from src.models.autoencoder import AE_CNN
from src.models.cnn import CNN
from src.models.rf import evaluate as evaluate_rf
from src.models.xgb import evaluate as evaluate_xgb
from sklearn.ensemble import RandomForestClassifier

import numpy as np


def get_config_space():
    cs = CS.ConfigurationSpace()

    # Use CNN or not
    # encode_images = CSH.CategoricalHyperparameter('encode_images',
    #                                               choices=['True', 'False'],
    #                                               default_value='True')
    model_type = CSH.CategoricalHyperparameter('model_type',
                                               choices=['NN', 'CNN', 'RF'],
                                               #          'XGB'],
                                               default_value='NN')

    # f_cnn_encode = CSF.ForbiddenAndConjunction(
    #         CSF.ForbiddenEqualsClause(encode_images, "True"),
    #         CSF.ForbiddenEqualsClause(model_type, "CNN"))

    # Configs for NN
    nn_lR = CSH.UniformFloatHyperparameter(name='nn_lR', lower=1e-4, upper=1e-1,
                                           log=True)
    nn_num_layers = CSH.UniformIntegerHyperparameter(name='nn_num_layers',
                                                     lower=1, upper=4)
    nn_hidden_dim = CSH.UniformIntegerHyperparameter(name='nn_hidden_dim',
                                                     lower=49, upper=300,
                                                     log=False)
    nn_act_f = CSH.CategoricalHyperparameter('nn_act_f',
                                             choices=['relu', 'tanh', 'elu'],
                                             default_value='relu')

    # Configs for RF
    rf_num_trees = CSH.UniformIntegerHyperparameter(name='rf_num_trees', lower=3,
                                                    upper=200, log=False,
                                                    default_value=50)
    # make later sure, that max_depth =0 is mapped to None!
    rf_max_depth = CSH.UniformIntegerHyperparameter(name='rf_max_depth', lower=0,
                                                    upper=20, default_value=0)
    rf_max_features = CSH.UniformFloatHyperparameter(name='rf_max_features',
                                                     lower=0.5, upper=1.0,
                                                     default_value=1.0)
    rf_min_samples_split = CSH.UniformIntegerHyperparameter(
        name='rf_min_samples_split', lower=1, upper=50, default_value=1)

    # Configs for XGB
    xgb_max_depth = CSH.UniformIntegerHyperparameter(name='xgb_max_depth',
                                                     lower=2, upper=20,
                                                     default_value=3)
    xgb_lR = CSH.UniformFloatHyperparameter(name='xgb_lR', lower=1e-5,
                                            upper=1e-1, log=True,
                                            default_value=1e-3)
    xgb_reg_lambda = CSH.UniformFloatHyperparameter(name='xgb_reg_lambda',
                                                    lower=1, upper=10,
                                                    default_value=1.)
    xgb_subsample = CSH.UniformFloatHyperparameter(name='xgb_subsample',
                                                   lower=0.5, upper=1.,
                                                   default_value=1.)
    xgb_n_estimators = CSH.Constant(name='xgb_n_estimators', value=10)
    """
    xgb_n_estimators = CSH.UniformIntegerHyperparameter(name='xgb_n_estimators',
                                                        lower=10, upper=500,
                                                        default_value=100)
    """
    # Configs for CNN
    cnn_lR = CSH.UniformFloatHyperparameter(name='cnn_lR', lower=1e-4,
                                            upper=1e-1, log=True)
    cnn_num_fc_layers = CSH.UniformIntegerHyperparameter(name='cnn_num_fc_layers',
                                                         lower=1, upper=4,
                                                         default_value=2)
    cnn_num_conv_layers = CSH.UniformIntegerHyperparameter(name='cnn_num_conv_layers',
                                                         lower=1, upper=4,
                                                         default_value=2)
    cnn_act_f = CSH.CategoricalHyperparameter('cnn_act_f',
                                              choices=['relu', 'tanh'],
                                              default_value='relu')
    cnn_batch_norm = CSH.CategoricalHyperparameter(name='cnn_batch_norm',
                                                   choices=['True', 'False'],
                                                   default_value='True')
    cnn_dropout = CSH.UniformFloatHyperparameter('cnn_dropout', lower=0.,
                                                 upper=0.5, log=False,
                                                 default_value=0.2)

    misc_hp = [model_type]
    nn_hp = [nn_lR, nn_num_layers, nn_hidden_dim, nn_act_f]
    rf_hp = [rf_num_trees, rf_max_depth, rf_max_features, rf_min_samples_split]
    # xgb_hp = [xgb_max_depth, xgb_lR, xgb_reg_lambda, xgb_subsample, xgb_n_estimators]
    cnn_hp = [cnn_lR, cnn_num_fc_layers, cnn_num_conv_layers,
              cnn_act_f, cnn_batch_norm, cnn_dropout]

    cs.add_hyperparameters(misc_hp + nn_hp + rf_hp + cnn_hp) # + xgb_hp)
    cs.add_conditions(
        [CSC.EqualsCondition(hp, model_type, 'NN') for hp in nn_hp]
        + [CSC.EqualsCondition(hp, model_type, 'RF') for hp in rf_hp]
        # + [CSC.EqualsCondition(hp, model_type, 'XGB') for hp in xgb_hp]
        + [CSC.EqualsCondition(hp, model_type, 'CNN') for hp in cnn_hp])
    # cs.add_forbidden_clause(f_cnn_encode)

    return cs


class BohbWorker(Worker):

    def __init__(self, options, *args, **kwargs):

        super(BohbWorker, self).__init__(*args, **kwargs)
        self.options = options
        self.data_dir = options.get('data_dir')
        self.out_path = options.get('out_path')
        self.max_budget = options.get('max_budget')
        self.device = options.get('device')
        self.latent_space = 100

        self.ae_model = AE_CNN(self.latent_space)
        self.ae_model.load_state_dict(
                torch.load('../out_path/results_ae_100/model.pt',
                           map_location=self.device))
        self.ae_model.to(self.device)

        train_dataset = K49(options.get('data_dir'),
                            True, transforms.ToTensor())
        test_dataset = K49(options.get('data_dir'),
                           False, transforms.ToTensor())

        self.train_loader = DataLoader(dataset=train_dataset,
                                       batch_size=options.get('batch_size'),
                                       shuffle=True)
        self.test_loader = DataLoader(dataset=test_dataset,
                                      batch_size=options.get('batch_size'),
                                      shuffle=False)

    def compute(self, config, budget, **kwargs):

        # distinguish model type
        model_type = config.get('model_type')

        # For NN + CNN: Enable savepoints.
        check_dir = self.out_path / 'checkpoints'
        check_dir.mkdir(exist_ok=True, parents=True)
        config_as_str = str(config).replace(':', '_').replace('\n', '') \
            .replace('{', '').replace('\'', '').replace('}', '') \
            .replace(' ', '')

        check_dir = check_dir / config_as_str
        fraction = budget / self.max_budget

        test_loss  = 100000000
        train_loss = 100000000
        precision = 100

        if model_type == 'NN':

            # nn_hp = [nn_lR, nn_num_layers, nn_hidden_dim, nn_act_f]
            classifier = Classifier(input_dim=100,
                                    hidden_dim=config['nn_hidden_dim'],
                                    output_dim=49,
                                    num_layers=config['nn_num_layers'],
                                    batch_norm=True,
                                    act_f=config['nn_act_f']).to(self.device)

            optimizer = optim.Adam(params=classifier.parameters(),
                                   lr=config['nn_lR'])
            loss_fct = CrossEntropyLoss().to(self.device)
            start_epoch = load_torch_checkpoint(check_dir=check_dir,
                                                optimizer=optimizer,
                                                model=classifier)

            for epoch in range(start_epoch, int(budget)):

                try:
                    train_loss = train(classifier=classifier,
                                       vae_model=self.ae_model,
                                       optimizer=optimizer,
                                       loss_fct=loss_fct,
                                       train_loader=self.train_loader,
                                       epoch=epoch+1,
                                       epochs=self.max_budget,
                                       device=self.device,
                                       out_path=check_dir)

                    test_loss, test_acc \
                        = test(classifier=classifier,
                               vae_model=self.ae_model,
                               loss_fct=loss_fct,
                               test_loader=self.test_loader,
                               epoch=epoch+1,
                               epochs=self.max_budget,
                               device=self.device)
                    precision = 100 - test_acc
                except Exception as e:
                    logger.exception(e)

        elif model_type == 'CNN':
            cnn = CNN(config=config,
                      input_shape=(self.train_loader.dataset.channels,
                                   self.train_loader.dataset.img_rows,
                                   self.train_loader.dataset.img_cols),
                      num_classes=self.train_loader.dataset.n_classes).to(self.device)

            optimizer = torch.optim.Adam(cnn.parameters(),
                                         lr=config.get('cnn_lR'))

            criterion = torch.nn.CrossEntropyLoss().to(self.device)

            start_epoch = load_torch_checkpoint(check_dir=check_dir,
                                                optimizer=optimizer,
                                                model=cnn)


            for epoch in range(start_epoch, int(budget)):
                train_acc, train_loss = \
                    cnn.train_fn(optimizer=optimizer,
                                 criterion=criterion,
                                 loader=self.train_loader,
                                 device=self.device,
                                 out_path=check_dir,
                                 epoch=epoch)

            test_acc = cnn.eval_fn(loader=self.test_loader, device=self.device)

            test_loss = 100 - test_acc
            precision = 100 - test_acc

        elif model_type == 'RF':
            # rf_hp = [rf_num_trees, rf_max_depth, rf_max_features,
            #         rf_min_samples_split]

            classifier = RandomForestClassifier(
                    n_estimators=config.get('rf_num_trees'),
                    max_depth=config.get('rf_max_depth'),
                    max_features=config.get('rf_max_features'),
                    min_samples_split=config.get('rf_min_samples_split'),
                    random_state=0,
                    verbose=0)

            try:
                feature_map = np.load(self.data_dir + '/train_feat.npy')
                labels = np.load(self.data_dir + '/train_labels.npy')
                data = feature_map, labels
            except FileNotFoundError:
                data = self.train_loader

            train_acc, train_mse, classifier = \
                evaluate_rf(classifier=classifier,
                            ae_model=self.ae_model,
                            loader=data,
                            latent_space=self.latent_space,
                            device=self.device,
                            fraction=fraction,
                            train=True)

            try:
                feature_map = np.load(self.data_dir + '/test_feat.npy')
                labels = np.load(self.data_dir + '/test_labels.npy')
                data = feature_map, labels
            except FileNotFoundError:
                data = self.test_loader

            test_acc, test_mse, classifier = \
                evaluate_rf(classifier=classifier,
                            ae_model=self.ae_model,
                            loader=data,
                            latent_space=self.latent_space,
                            device=self.device,
                            fraction=1.,
                            train=False)

            test_loss = test_mse
            train_loss = train_acc
            precision = 100 - test_acc

        elif model_type == 'XGB':

            classifier = xgb.XGBClassifier(
                    max_depth=config.get('xgb_max_depth'),
                    learning_rate=config.get('xgb_lR'),
                    reg_lambda=config.get('xgb_reg_lambda'),
                    subsample=config.get('xgb_subsample'),
                    n_estimators=config.get('xgb_n_estimators'),
                    verbosity=0
                    )
            try:
                feature_map = np.load(self.data_dir + '/train_feat.npy')
                labels = np.load(self.data_dir + '/train_labels.npy')
                data = feature_map, labels
            except FileNotFoundError:
                data = self.train_loader

            train_acc, train_mse, classifier = \
                evaluate_xgb(classifier=classifier,
                             ae_model=self.ae_model,
                             loader=data,
                             latent_space=self.latent_space,
                             device=self.device,
                             fraction=fraction,
                             train=True)

            try:
                feature_map = np.load(self.data_dir + '/test_feat.npy')
                labels = np.load(self.data_dir + '/test_labels.npy')
                data = feature_map, labels
            except FileNotFoundError:
                data = self.test_loader

            test_acc, test_mse, classifier = \
                evaluate_xgb(classifier=classifier,
                             ae_model=self.ae_model,
                             loader=data,
                             latent_space=self.latent_space,
                             device=self.device,
                             fraction=1.,
                             train=False)

            test_loss = test_mse
            train_loss = train_acc
            precision = 100 - test_acc

        else:
            raise ValueError('Unknown ModelType {}'.format(model_type))

        return ({'loss': float(precision),
                 'info': {'train_loss': float(train_loss),
                          'test_loss': float(test_loss),
                          'test_accuracy': float(100 - precision),
                          'config': config}
            })


def bohb_master(worker, latent_space, n_iterations, config_space,
                min_budget, max_budget, batch_size,
                out_path, device, data_dir,
                opts=None, nic_name=None, run_id='opt_fun', array_id=1):

    opts = opts or {}
    config_space = config_space or get_config_space()

    options = dict(out_path=out_path,
                   device=device,
                   latent_space=latent_space,
                   batch_size=batch_size,
                   data_dir=data_dir,
                   max_budget=max_budget)
    options.update(opts)

    if array_id == 1:

        result_logger = hpres.json_result_logger(directory=str(out_path),
                                                 overwrite=True)

        ns = hpns.NameServer(run_id=run_id,
                             nic_name=nic_name,
                             working_directory=str(out_path))
        ns_host, ns_port = ns.start()

        if worker is None:
            w = BohbWorker(nameserver=ns_host,
                           nameserver_port=ns_port,
                           run_id=run_id,
                           options=options
                           )
        else:

            w = worker(nameserver=ns_host,
                       nameserver_port=ns_port,
                       run_id=run_id,
                       options=options
                       )

        w.run(background=True)

        bohb = BOHB(configspace=config_space,
                    run_id=run_id,
                    host=ns_host,
                    nameserver=ns_host,
                    nameserver_port=ns_port,
                    min_budget=min_budget,
                    max_budget=max_budget,
                    result_logger=result_logger
                    )

        result = bohb.run(n_iterations=n_iterations)
        bohb.shutdown(shutdown_workers=True)
        ns.shutdown()

        with open(out_path / 'results.pkl', 'wb') as f:
            import pickle
            pickle.dump(result, f)

        id2config = result.get_id2config_mapping()
        incumbent = result.get_incumbent_id()
        inc_value = result.get_runs_by_id(incumbent)[-1]['loss']
        inc_cfg = id2config[incumbent]['config']

        logger.info('Inc Config:\n{}\nwith Perfomance: {:.2f}'
                    .format( inc_cfg, inc_value))

    else:
        from time import sleep
        sleep(25)

        host = hpns.nic_name_to_host(nic_name)
        if worker is None:
            w = BohbWorker(run_id=run_id,
                           host=host,
                           options=options
                           )
        else:
            w = worker(run_id=run_id,
                       host=host,
                       options=options
                       )

        w.load_nameserver_credentials(str(out_path))
        w.run(background=False)


def load_torch_checkpoint(check_dir, optimizer, model):
    try:
        check_dir.parent.mkdir(exist_ok=True, parents=True)
        state = torch.load(check_dir)
        optimizer.load_state_dict(state['optimizer'])
        model.load_state_dict(state['state_dict'])
        start_epoch = state['epoch']
        logger.info('OLD CONFIG LOADED!')
    except FileNotFoundError:
        start_epoch = 0

    return start_epoch


def parse_args():
    parser = argparse.ArgumentParser(description='Jo')
    parser.add_argument('--run_id',
                        help='unique id to identify the HPB run.',
                        default='Opt_run',
                        type=str)

    parser.add_argument('--nic_name',
                        help='name of the Network Interface Card',
                        default=None,
                        type=str)

    parser.add_argument('--array_id',
                        help='SGE array id to tread one job array as a HPB run',
                        default=1,
                        type=int)

    parser.add_argument('--data_path',
                        help='Path to the data',
                        default='./data',
                        type=str)

    parser.add_argument('--out_path',
                        help='Output path',
                        default='./out_path',
                        type=str)

    parser.add_argument('--no_cuda',
                        help='True or False if no cuda',
                        default='False',
                        type=str)

    args = parser.parse_args()
    return args
