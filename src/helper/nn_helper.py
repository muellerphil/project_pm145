import torch


def get_activation_function_obj(act_f):

    if act_f.lower() == 'relu':
        act_f = torch.nn.ReLU()
    elif act_f.lower() == 'tanh':
        act_f = torch.nn.Tanh()
    elif act_f.lower() == 'elu':
        act_f = torch.nn.ELU()
    else:
        raise ValueError('Act_f not in [relu, tanh, elu] but {}'
                         .format(act_f))

    return act_f