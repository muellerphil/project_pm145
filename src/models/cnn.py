import numpy as np
import torch
import torch.nn.functional as F
import torch.nn as nn
from torch.autograd import Variable
from tqdm import tqdm

from src.helper.utils import AvgrageMeter, accuracy


class CNN(nn.Module):

    def __init__(self, config, input_shape=(1, 28, 28), num_classes=10):
        super(CNN, self).__init__()

        self.cnn_lR = config.get('cnn_lR')
        self.cnn_num_fc_layers = config.get('cnn_num_fc_layers')
        self.cnn_num_conv_layers = config.get('cnn_num_conv_layers')
        # self.cnn_hidden_dim = config.get('cnn_hidden_dim')
        self.cnn_act_f = config.get('cnn_act_f')
        self.cnn_batch_norm = config.get('cnn_batch_norm')
        self.cnn_dropout = config.get('cnn_dropout')

        if self.cnn_act_f.lower() == 'relu':
            self.cnn_act_f = torch.nn.ReLU()
        elif self.cnn_act_f.lower() == 'tanh':
            self.cnn_act_f = torch.nn.Tanh()
        elif self.cnn_act_f.lower() == 'elu':
            self.cnn_act_f = torch.nn.ELU()
        else:
            raise ValueError('Act_f not in [relu, tanh, elu] but {}'
                             .format(self.cnn_act_f))

        kernel_size = 2
        in_channels = input_shape[0]
        out_channels = 4
        layers = []
        for i in range(self.cnn_num_conv_layers):
            c = nn.Conv2d(in_channels,
                          out_channels,
                          kernel_size=kernel_size,
                          stride=2,
                          padding=1
                          )
            a = self.cnn_act_f
            p = nn.MaxPool2d(kernel_size=2, stride=1)
            layers.extend([c, a, p])
            in_channels = out_channels
            out_channels *= 2

        self.conv_layers = nn.Sequential(*layers)
        self.output_size = num_classes

        layers = []
        # self.fc_layers = nn.ModuleList()
        n_in = self._get_conv_output(input_shape)
        n_out = 256
        for i in range(self.cnn_num_fc_layers):

            fc = nn.Linear(int(n_in), int(n_out))
            a = self.cnn_act_f
            do = nn.Dropout(p=self.cnn_dropout)
            bn = nn.BatchNorm1d(int(n_out))
            n_in = n_out
            n_out /= 2

            if self.cnn_batch_norm:
                layers.extend([fc, a, do, bn])
            else:
                layers.extend([fc, a, do])

            # self.fc_layers += [fc]
            # self.bn.append(nn.BatchNorm1d(int(n_out)))

        self.fc_layers = nn.Sequential(*layers)
        self.last_fc = nn.Linear(int(n_in), self.output_size)
        # self.dropout = nn.Dropout(p=self.cnn_dropout)

    # generate input sample and forward to get shape
    def _get_conv_output(self, shape):
        bs = 1
        input = Variable(torch.rand(bs, *shape))
        output_feat = self.conv_layers(input)
        n_size = output_feat.data.view(bs, -1).size(1)
        return n_size

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(x.size(0), -1)
        x = self.fc_layers(x)
        # for i, fc_layer in enumerate(self.fc_layers):
        #    x = fc_layer(x)
        #    x = self.cnn_act_f(x)
        #    x = self.dropout(x)
        #    x = self.bn[i](x) if self.batch_norm else x
        #    # x = self.dropout(F.relu(fc_layer(x)))
        x = self.last_fc(x)
        return x

    def train_fn(self, optimizer, criterion, loader, device, out_path, epoch):
        """
        Training method
        :param optimizer: optimization algorithm
        :criterion: loss function
        :param loader: data loader for either training or testing set
        :param device: torch device
        :param train: boolean to indicate if training or test set is used
        :return: (accuracy, loss) on the data
        """
        score = AvgrageMeter()
        objs = AvgrageMeter()
        self.train()

        t = tqdm(loader)
        for images, labels in t:
            images = images.to(device)
            labels = labels.to(device)

            optimizer.zero_grad()
            logits = self(images)
            loss = criterion(logits, labels)
            loss.backward()
            optimizer.step()

            acc, _ = accuracy(logits, labels, topk=(1, 5))
            n = images.size(0)
            objs.update(loss.item(), n)
            score.update(acc.item(), n)

            t.set_description('(=> Training) Loss: {:.4f} Accuracy: {:.4f}'
                              .format(objs.avg, score.avg))

        state = {
            'epoch': epoch + 1, 'state_dict': self.state_dict(),
            'optimizer': optimizer.state_dict(),
            }

        torch.save(state, out_path)

        return score.avg, objs.avg

    def eval_fn(self, loader, device, train=False):
        """
        Evaluation method
        :param loader: data loader for either training or testing set
        :param device: torch device
        :param train: boolean to indicate if training or test set is used
        :return: accuracy on the data
        """
        score = AvgrageMeter()
        self.eval()

        t = tqdm(loader)
        with torch.no_grad():  # no gradient needed
            for images, labels in t:
                images = images.to(device)
                labels = labels.to(device)

                outputs = self(images)
                acc, _ = accuracy(outputs, labels, topk=(1, 5))
                score.update(acc.item(), images.size(0))

                t.set_description('(=> Test) Score: {:.4f}'.format(score.avg))

        return score.avg

