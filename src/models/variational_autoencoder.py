#!/usr/bin/env python
# coding: utf-8
import pickle
import matplotlib.pyplot as plt
import torch
from pathlib import Path
from tqdm import tqdm as tqdm
from skimage import io, transform
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision.utils import save_image


from src.helper.utils import AvgrageMeter
from src.helper.nn_helper import get_activation_function_obj
from src.helper.datasets import K49


class VAE_CNN(nn.Module):
    def __init__(self, latent_space_size, act_f):
        super(VAE_CNN, self).__init__()

        # Encoder
        # input: batch_size x 1 x 28 x 28
        # conv1: batch_size x 16 x 28 x 28
        self.conv1 = nn.Conv2d(1, 16, kernel_size=3, stride=1,
                               padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(16)
        # conv2: batch_size x 32 x 28 x 28
        self.conv2 = nn.Conv2d(16, 32, kernel_size=3, stride=1,
                               padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(32)
        # max_pool2: batch_size x 32 x 14 x 14
        self.max_pool2 = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        # conv3: batch_size x 64 x 14 x 14
        self.conv3 = nn.Conv2d(32, 64, kernel_size=3, stride=1,
                               padding=1, bias=False)
        self.bn3 = nn.BatchNorm2d(64)
        # conv4: batch_size x 16 x 14 x 14
        self.conv4 = nn.Conv2d(64, 16, kernel_size=3, stride=1,
                               padding=1, bias=False)
        self.bn4 = nn.BatchNorm2d(16)
        # max_pool4: batch_size x 16 x 7 x 7
        self.max_pool4 = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        # Latent vectors mu and sigma
        self.fc1 = nn.Linear(7 * 7 * 16, latent_space_size)
        self.fc_bn1 = nn.BatchNorm1d(latent_space_size)
        self.fc21 = nn.Linear(latent_space_size, latent_space_size)
        self.fc22 = nn.Linear(latent_space_size, latent_space_size)

        # Sampling vector
        self.fc3 = nn.Linear(latent_space_size, latent_space_size)
        self.fc_bn3 = nn.BatchNorm1d(latent_space_size)
        self.fc4 = nn.Linear(latent_space_size, 7 * 7 * 16)
        self.fc_bn4 = nn.BatchNorm1d(7 * 7  * 16)
        # --> TO Decoder: batch_size x 2048
        # --> In Decoder  batch_size x 7*7*16
        # --> In Decoder  batch_size x 16 x 7 x 7

        # Decoder
        self.conv5 = nn.ConvTranspose2d(16, 64, kernel_size=3, stride=2,
                                        padding=1, output_padding=1, bias=False)
        self.bn5 = nn.BatchNorm2d(64)
        self.conv6 = nn.ConvTranspose2d(64, 32, kernel_size=3, stride=1,
                                        padding=1, bias=False)
        self.bn6 = nn.BatchNorm2d(32)
        self.conv7 = nn.ConvTranspose2d(32, 16, kernel_size=3, stride=2,
                                        padding=1, output_padding=1, bias=False)
        self.bn7 = nn.BatchNorm2d(16)
        self.conv8 = nn.ConvTranspose2d(16, 1, kernel_size=3, stride=1,
                                        padding=1, bias=False)

        self.act_f = get_activation_function_obj(act_f)
        self.clamp = False

    def encode(self, x):
        conv1 = self.act_f(self.bn1(self.conv1(x)))
        conv2 = self.act_f(self.max_pool2(self.bn2(self.conv2(conv1))))
        conv3 = self.act_f(self.bn3(self.conv3(conv2)))
        conv4 = self.act_f(self.max_pool4(self.bn4(self.conv4(conv3))).view(-1, 7 * 7 * 16))

        fc1 = self.act_f(self.fc_bn1(self.fc1(conv4)))

        r1 = self.fc21(fc1)
        r2 = self.fc22(fc1)

        return r1, r2

    def reparameterize(self, mu, logvar):
        if self.training:
            std = logvar.mul(0.5).exp_()
            eps = Variable(std.data.new(std.size()).normal_())
            return eps.mul(std).add_(mu)
        else:
            return mu

    def decode(self, z):
        fc3 = self.act_f(self.fc_bn3(self.fc3(z)))
        fc4 = self.act_f(self.fc_bn4(self.fc4(fc3))).view(-1, 16, 7, 7)

        conv5 = self.act_f(self.bn5(self.conv5(fc4)))
        conv6 = self.act_f(self.bn6(self.conv6(conv5)))
        conv7 = self.act_f(self.bn7(self.conv7(conv6)))
        conv8 = self.conv8(conv7).view(-1, 1, 28, 28)
        
        if self.clamp:
            conv8.clamp_(0, 1)

        return conv8

    def forward(self, x):
        mu, logvar = self.encode(x)

        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar


class customLoss(nn.Module):
    def __init__(self):
        super(customLoss, self).__init__()
        self.mse_loss = nn.MSELoss(reduction="sum")

    def forward(self, x_recon, x, mu, logvar):

        loss_MSE = self.mse_loss(x_recon, x)
        loss_KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

        return loss_MSE + loss_KLD


def train(model, optimizer, epoch, train_loader,
          loss_mse, epochs, out_path, device):
    t = tqdm(train_loader)
    model.train()
    objs = AvgrageMeter()

    for batch_idx, (images, labels) in enumerate(t):
        images = images.to(device)

        optimizer.zero_grad()

        # if epoch == 10:
        #     model.clamp = True

        result = model(images)
        if type(result) is tuple:
            recon_batch, mu, logvar = result
            loss = loss_mse(recon_batch, images, mu, logvar)
        else:
            loss = loss_mse(result, images)

        # recon_batch, mu, logvar = model(images)
        # loss = loss_mse(recon_batch, images, mu, logvar)
        loss.backward()
        optimizer.step()
        
        n = images.size(0)
        objs.update(loss.item(), n)
        t.set_description('(=> Training Epoch [{:3.0f}|{:3.0f}])) Loss: {:.4f}'
                          .format(epoch, epochs, objs.avg))

    state = {'epoch': epoch + 1,
             'state_dict': model.state_dict(),
             'optimizer': optimizer.state_dict(),
             }

    torch.save(state, out_path)
    return objs.avg


def test(model, epoch, test_loader, loss_mse, epochs, batch_size, out_path, device):
    t = tqdm(test_loader)
    model.eval()
    objs = AvgrageMeter()

    with torch.no_grad():
        for i, (images, labels) in enumerate(t):
            images = images.to(device)

            recon_batch = model(images)
            if type(recon_batch) is tuple:
                recon_batch, mu, logvar = recon_batch
                loss = loss_mse(recon_batch, images, mu, logvar)
            else:
                loss = loss_mse(recon_batch, images)

            objs.update(loss.item(), images.size(0))

            if i == 0:
                n = min(images.size(0), 8)
                comparison = \
                    torch.cat([images[:n],
                               recon_batch.view(batch_size, 1, 28, 28)[:n]])
                comparison = torch.clamp(comparison, 0, 1)

                save_image(comparison.cpu(),
                           out_path / ('images/reconstruction_'
                           + str(epoch) + '.png'), nrow=n)
            t.set_description('(=> TEST Epoch [{:3.0f}|{:3.0f}])) Loss: {:.4f}'
                              .format(epoch, epochs, objs.avg))

    return objs.avg


if __name__ == '__main__':

    config_path = Path('/home/philipp/Documents/Code/project/'
                       'out_path/BOHB_opt/VarAutoEncoder/results.pkl')

    result = pickle.load(config_path.open('rb'))
    id2config = result.get_id2config_mapping()
    incumbent = result.get_incumbent_id()
    inc_cfg = id2config[incumbent]['config']

    batch_size = 64
    epochs = 70
    latent_space_size = 100
    no_cuda = False
    cuda = not no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    print('Device ', device, torch.cuda.is_available())
    kwargs = {'num_workers': 1, 'pin_memory': True} if cuda else {}

    seed = 1
    log_interval = 50
    load_model = False
    warm_start = True
    data_dir = '/home/philipp/Documents/Code/project/data'

    out_path = Path('/home/philipp/Documents/Code/project/out_path/TrainedNets/'
                    'VarAutoEncoder_{}'.format(inc_cfg.get('latent_space')))
    out_path.mkdir(exist_ok=True, parents=True)
    (out_path / 'images').mkdir(exist_ok=True)

    torch.manual_seed(seed)
    data_augmentations = transforms.ToTensor()

    train_dataset = K49(data_dir, True, data_augmentations)
    test_dataset = K49(data_dir, False, data_augmentations)

    train_loader = DataLoader(dataset=train_dataset,
                              batch_size=batch_size,
                              shuffle=True)

    test_loader = DataLoader(dataset=test_dataset,
                             batch_size=batch_size,
                             shuffle=False)

    iterations = 5
    import numpy as np
    hist_train = np.zeros((iterations, epochs))
    hist_valid = np.zeros((iterations, epochs))

    for it in range(iterations):
        model = VAE_CNN(latent_space_size=inc_cfg.get('latent_space'),
                        act_f=inc_cfg.get('act_f')).to(device)

        weight_decay = inc_cfg.get('weight_decay')
        weight_decay = 0 if weight_decay < 1e-4 else weight_decay

        optimizer = optim.Adam(model.parameters(),
                               lr=inc_cfg.get('lR'),
                               weight_decay=weight_decay)
        loss_custom = customLoss().to(device)
        loss_mse = nn.MSELoss().to(device)

        # ######################## TRAIN ##################################### #
        val_losses = []
        train_losses = []

        for epoch in range(1, epochs + 1):
            if warm_start:
                load_model = (epoch == 1)

            if not load_model:
                train_losses.append(
                        train(model=model,
                              optimizer=optimizer,
                              epoch=epoch,
                              train_loader=train_loader,
                              loss_mse=loss_custom,
                              epochs=epochs,
                              out_path=out_path / 'model.pt',
                              device=device))
            else:
                model.load_state_dict(torch.load(out_path / 'model.pt'))

            val_losses.append(
                    test(model=model,
                         epoch=epoch,
                         test_loader=test_loader,
                         loss_mse=loss_mse,
                         epochs=epochs,
                         batch_size=batch_size,
                         out_path=out_path,
                         device=device))

            with torch.no_grad():
                sample = torch.randn(64, inc_cfg.get('latent_space')).to(device)
                sample = model.decode(sample).to(device)
                save_image(sample.view(64, 1, 28, 28),
                           out_path / 'images' / (
                                       'sample_' + str(epoch) + '.png'))

        hist_train[it] = np.array(train_losses)
        hist_valid[it] = np.array(val_losses)

    hist_train_m = hist_train.mean(axis=0)
    hist_valid_m = hist_valid.mean(axis=0)

    hist_train_std = 2 * hist_train.std(axis=0)
    hist_valid_std = 2 * hist_valid.std(axis=0)

    np.save(out_path / 'hist_train.npy', hist_train)
    np.save(out_path / 'hist_valid.npy', hist_valid)

    plt.figure(figsize=(15, 10))
    x_range = range(1, len(hist_train_m) + 1)
    plt.plot(x_range, hist_train_m, color='blue')
    plt.plot(x_range, hist_valid_m, color='orange')

    plt.fill_between(x_range,
                     hist_train_m - hist_train_std,
                     hist_train_m + hist_train_std, color='skyblue',
                     alpha=0.4)

    plt.fill_between(x_range,
                     hist_valid_m - hist_valid_std,
                     hist_valid_m + hist_valid_std, color='sandybrown',
                     alpha=0.4)

    plt.title("Validation loss and loss per epoch", fontsize=18)
    plt.xlabel("epoch", fontsize=18)
    plt.ylabel("loss", fontsize=18)
    plt.legend(['Training Loss', 'Validation Loss'], fontsize=14)
    plt.savefig(out_path / 'images' / 'curve')
