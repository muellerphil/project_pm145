import numpy as np
import pickle
import torch
import matplotlib.pyplot as plt
from tqdm import tqdm as tqdm
from torch import nn, optim
from torch.nn import functional as F, CrossEntropyLoss
from torchvision import transforms
from torch.utils.data import DataLoader
from src.helper.datasets import K49
from src.darts.model_search import Network as DartsEncoder
from src.helper.utils import AvgrageMeter, accuracy
from pathlib import Path
from src.helper.nn_helper import get_activation_function_obj
from src.models.nn import Classifier


def train(classifier, darts_ae, optimizer, loss_fct,
          train_loader, epoch, epochs, device, out_path, debug=True):

    t = tqdm(train_loader) if debug else test_loader
    classifier.train()
    train_loss = 0
    objs = AvgrageMeter()
    acc_objs = AvgrageMeter()

    for batch_idx, (images, labels) in enumerate(t):
        images = images.to(device)
        labels = labels.to(device)

        if darts_ae is not None:
            with torch.no_grad():
                darts_ae.eval()
                feat_map = darts_ae.encode(images, eval=True)
                if type(feat_map) is tuple:
                    feat_map, _ = feat_map
        else:
            feat_map, labels = images.float(), labels.reshape((-1, 1)).float()

        optimizer.zero_grad()

        prediction = classifier(feat_map)

        loss = loss_fct(prediction, labels)
        loss.backward()

        train_loss += loss.item()
        optimizer.step()

        n = images.size(0)
        objs.update(loss.item(), n)

        if darts_ae is None:
            labels = labels.long()
        acc = accuracy(prediction, labels)[0]
        acc_objs.update(acc.item(), n)

        if debug:
            t.set_description('(=> Training Epoch [{:3.0f}|{:3.0f}]))'
                              ' Loss: {:.4f}: Acc {:.2f}'
                              .format(epoch, epochs, objs.avg, acc_objs.avg))

    state = {
        'epoch': epoch + 1, 'state_dict': classifier.state_dict(),
        'optimizer': optimizer.state_dict(),
        }

    torch.save(state, out_path)
    return objs.avg, acc_objs.avg


def test(classifier, darts_ae, loss_fct,
         test_loader, epoch, epochs, device, debug=True):

    t = tqdm(test_loader) if debug else test_loader
    classifier.eval()
    test_loss = 0
    objs = AvgrageMeter()
    acc_objs = AvgrageMeter()

    for batch_idx, (images, labels) in enumerate(t):
        images = images.to(device)
        labels = labels.to(device)

        if darts_ae is not None:
            with torch.no_grad():
                darts_ae.eval()
                feat_map = darts_ae.encode(images, eval=True)
                if type(feat_map) is tuple:
                    feat_map, _ = feat_map

        # We pass the already encoded data to the learning process!
        else:
            feat_map, labels = images, labels

        prediction = classifier(feat_map)

        loss = loss_fct(prediction, labels)
        test_loss += loss.item()

        n = images.size(0)
        objs.update(loss.item(), n)

        acc = accuracy(prediction, labels)[0]
        acc_objs.update(acc.item(), n)

        if debug:
            t.set_description('(=> TEST Epoch [{:3.0f}|{:3.0f}]))'
                              ' Loss: {:.4f}: Acc {:.2f}'
                              .format(epoch, epochs, objs.avg, acc_objs.avg))

    return objs.avg, acc_objs.avg


if __name__ == "__main__":

    no_cuda = False
    cuda = not no_cuda and torch.cuda.is_available()
    data_dir = '/home/philipp/Documents/Code/project/data'
    out_path = Path('/home/philipp/Documents/Code/project/out_path/TrainedNets/'
                    'ClassifierDartsEncoder_{}'.format(100))

    out_path.mkdir(exist_ok=True, parents=True)
    (out_path / 'images').mkdir(exist_ok=True)

    load_model = False
    warm_start = False

    batch_size = 64
    epochs = 20
    seed = 1
    log_interval = 50

    torch.manual_seed(seed)
    data_augmentations = transforms.ToTensor()
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {'num_workers': 1, 'pin_memory': True} if cuda else {}

    # Load AE Config
    latent_space = 100
    act_f = 'tanh'
    darts_encoder = DartsEncoder(device=device, nodes=4)
    state = torch.load('./out_path/TrainedNets/Darts_4_nodes/state_fixed_arch.pt', map_location=device)
    darts_encoder.load_state_dict(state['state_dict'])
    old_alphas = state['alphas']
    darts_encoder.arch_parameters = old_alphas
    darts_encoder.alphas_normal = old_alphas[0]
    darts_encoder.alphas_reduce = old_alphas[1]
    darts_encoder.alphas_upsample = old_alphas[2]
    darts_encoder.to(device)

    # Load Classifier Config
    try:
        config_path = Path('/home/philipp/Documents/Code/project/'
                           'out_path/BOHB_opt/'
                           'ClassifierDartsAutoEncoder/results.pkl')
        result = pickle.load(config_path.open('rb'))
        id2config = result.get_id2config_mapping()
        incumbent = result.get_incumbent_id()
        config = id2config[incumbent]['config']
    except FileNotFoundError:
        config = dict(hidden_dim=250, num_layers=2, act_f='relu',
                      weight_decay=0, lR=0.001)

    iterations = 5
    hist_train = np.zeros((iterations, epochs))
    hist_train_acc = np.zeros((iterations, epochs))
    hist_valid = np.zeros((iterations, epochs))
    hist_valid_acc = np.zeros((iterations, epochs))

    for it in range(iterations):
        print('ITERATION ', it)

        classifier = Classifier(input_dim=latent_space,
                                hidden_dim=config.get('hidden_dim'),
                                output_dim=49,
                                num_layers=config.get('num_layers'),
                                batch_norm=True,
                                act_f=config.get('act_f')).to(device)
        if it == 0:
            print('Classifier: ', sum([p.numel() for p in classifier.parameters()]))
            print('AutoEnc: ', sum([p.numel() for p in darts_encoder.parameters()]))

        optimizer = optim.Adam(classifier.parameters(),
                               lr=config['lR'],
                               weight_decay=config['weight_decay'])
        loss_fct = CrossEntropyLoss().to(device)

        train_dataset = K49(data_dir, True, data_augmentations)
        test_dataset = K49(data_dir, False, data_augmentations)

        # Make data batch iterable
        # Could modify the sampler to not uniformly random sample
        train_loader = DataLoader(dataset=train_dataset,
                                  batch_size=batch_size,
                                  shuffle=True)

        test_loader = DataLoader(dataset=test_dataset,
                                 batch_size=batch_size,
                                 shuffle=False)

        # ######################## TRAIN ##################################### #
        train_losses = []
        train_accuracies = []
        val_losses = []
        val_accuracies = []

        for epoch in range(0, epochs):

            train_loss, train_accuracy = train(classifier=classifier,
                                               darts_ae=darts_encoder,
                                               optimizer=optimizer,
                                               loss_fct=loss_fct,
                                               train_loader=train_loader,
                                               epoch=epoch+1,
                                               epochs=epochs,
                                               device=device,
                                               out_path=out_path / 'model.pt')

            test_loss, test_accuracy = test(classifier=classifier,
                                            darts_ae=darts_encoder,
                                            loss_fct=loss_fct,
                                            test_loader=test_loader,
                                            epoch=epoch+1,
                                            epochs=epochs,
                                            device=device)

            train_losses.append(train_loss)
            train_accuracies.append(train_accuracy)
            val_losses.append(test_loss)
            val_accuracies.append(test_accuracy)
        print('write train losses in np array')
        hist_train[it] = np.array(train_losses)
        hist_train_acc[it] = np.array(train_accuracies)
        hist_valid[it] = np.array(val_losses)
        hist_valid_acc[it] = np.array(val_accuracies)
        print('write train losses in np array : DONE ')

    # ######################## PLOTTING ###################################### #
    hist_train_m = hist_train.mean(axis=0)
    hist_train_acc_m = hist_train_acc.mean(axis=0)
    hist_valid_m = hist_valid.mean(axis=0)
    hist_valid_acc_m = hist_valid_acc.mean(axis=0)

    hist_train_std = 2 * hist_train.std(axis=0)
    hist_train_acc_std = 2 * hist_train_acc.std(axis=0)
    hist_valid_std = 2 * hist_valid.std(axis=0)
    hist_valid_acc_std = 2 * hist_valid_acc.std(axis=0)

    np.save(out_path / 'hist_train.npy', hist_train)
    np.save(out_path / 'hist_train_acc.npy', hist_train_acc)
    np.save(out_path / 'hist_valid.npy', hist_valid)
    np.save(out_path / 'hist_valid_acc.npy', hist_valid_acc)

    x_range = range(1, len(hist_train_m) + 1)

    plt.figure(figsize=(15, 10))
    plt.plot(x_range, hist_train_m, color='blue')
    plt.plot(x_range, hist_valid_m, color='orange')

    plt.fill_between(x_range,
                     hist_train_m - hist_train_std,
                     hist_train_m + hist_train_std, color='skyblue',
                     alpha=0.4)

    plt.fill_between(x_range,
                     hist_valid_m - hist_valid_std,
                     hist_valid_m + hist_valid_std, color='sandybrown',
                     alpha=0.4)

    plt.title("Validation loss and loss per epoch", fontsize=18)
    plt.xlabel("epoch", fontsize=18)
    plt.ylabel("mse loss", fontsize=18)
    plt.legend(['Training Loss', 'Validation Loss'], fontsize=14)
    plt.savefig(out_path / 'images' / 'curve_loss')

    # ##### #### ### ### ## Plot Accuracy ## ##  # ## # ## ### ### ## ### ### ##
    plt.figure(figsize=(15, 10))
    plt.plot(x_range, hist_train_acc_m, color='blue')
    plt.plot(x_range, hist_valid_acc_m, color='orange')

    plt.fill_between(x_range,
                     hist_train_acc_m - hist_train_acc_std,
                     hist_train_acc_m + hist_train_acc_std, color='skyblue',
                     alpha=0.4)

    plt.fill_between(x_range,
                     hist_valid_acc_m - hist_valid_acc_std,
                     hist_valid_acc_m + hist_valid_acc_std, color='sandybrown',
                     alpha=0.4)

    plt.title("Accuracy per epoch", fontsize=18)
    plt.xlabel("epoch", fontsize=18)
    plt.ylabel("accuracy in %", fontsize=18)
    plt.legend(['Training Accuracy', 'Validation Accuracy'], fontsize=14)
    plt.savefig(out_path / 'images' / 'curve_accuracy')

