import numpy as np
import torch

from sklearn.metrics import mean_squared_error
from src.helper.utils import accuracy
import logging
logger = logging.getLogger('RF')


def evaluate(classifier, ae_model, loader, latent_space, device, fraction,
             train=False):
    if type(loader) is tuple:
        feature_map, sorted_labels = loader
    else:
        logger.info('Calculate Feature Map')

        unsorted_labels = loader.dataset.labels
        feature_map = np.zeros([unsorted_labels.shape[0], latent_space])
        sorted_labels = np.zeros(unsorted_labels.shape)

        for batch_idx, (images, labels_batch) in enumerate(loader):
            images = images.to(device)

            with torch.no_grad():
                ae_model.eval()
                feat_map = ae_model.encode(images)
                if type(feat_map) is tuple:
                    feat_map, _ = feat_map

                start_idx = batch_idx * loader.batch_size
                end_idx = min(len(loader.dataset),
                              start_idx + loader.batch_size)
                feature_map[start_idx:end_idx] = feat_map.reshape(
                        (-1, latent_space))
                sorted_labels[start_idx:end_idx] = labels_batch

        logger.info('save')
        if train:
            np.save('../data/train_feat.npy', feature_map)
            np.save('../data/train_labels.npy', sorted_labels)
        else:
            np.save('../data/test_feat.npy', feature_map)
            np.save('../data/test_labels.npy', sorted_labels)
        logger.info('save_finish')

    if train:
        idx = np.arange(len(feature_map))
        idx = np.random.choice(idx, int(len(idx) * fraction), replace=False)
        logger.info('Use {:.2f} % datapoint'.format(fraction * 100))
        logger.info('start training with {}/{} datapoints'.format(len(idx),
                                                            len(sorted_labels)))
        classifier = classifier.fit(feature_map[idx],
                                    sorted_labels[idx].reshape((-1,)))

    prediction = classifier.predict(feature_map)
    acc = (prediction == sorted_labels).mean() * 100

    mse = mean_squared_error(prediction, sorted_labels)
    return acc, mse, classifier
