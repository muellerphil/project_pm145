#!/bin/bash
#SBATCH -p ml_cpu-ivy # partition (queue)
#SBATCH --mem 4000 # memory pool for all cores (4GB)
#SBATCH -t 2-00:00 # time (D-HH:MM)
#SBATCH -c 1 # number of cores
#SBATCH -a 1-4
#SBATCH -o ./logs/out.%x.%N.%j.log # STDOUT  (the folder log has to be created prior to running or this won't work)
#SBATCH -e log/err.%x.%N.%j.log # STDERR  (the folder log has to be created prior to running or this won't work)
#SBATCH -J enc_opt # sets the job name. If not specified, the file name will be used as job name

echo "Workingdir: $PWD";
echo "Started at $(date)";

export OPENBLAS_NUM_THREADS=1

# Job to perform
/home/muelleph/miniconda3/envs/project_env/bin/python -m src.models.autoencoder

# Print some Information about the end-time to STDOUT
echo "DONE";
echo "Finished at $(date)";
