#!/bin/bash
#SBATCH -p ml_cpu-ivy # partition (queue)
#SBATCH --mem 4000 # memory pool for all cores (4GB)
#SBATCH -t 2-00:00 # time (D-HH:MM)
#SBATCH -c 1 # number of cores
#SBATCH -a 1-4
#SBATCH -o ./logs/out.%j.log # STDOUT  (the folder log has to be created prior to running or this won't work)
#SBATCH -e ./logs/err.%j.log # STDERR  (the folder log has to be created prior to running or this won't work)
#SBATCH -J enc_opt # sets the job name. If not specified, the file name will be used as job name

echo "Workingdir: $PWD";
echo "Started at $(date)";
echo "Running job $SLURM_JOB_NAME using $SLURM_JOB_CPUS_PER_NODE cpus per node with given
JID $SLURM_JOB_ID on queue $SLURM_JOB_PARTITION";

export OPENBLAS_NUM_THREADS=1

# Job to perform
/home/muelleph/miniconda3/envs/project_env/bin/python -m src.bohb_opt.bohb_variational_autoencoder \
    --run_id vae_opt --nic_name eth0  --array_id  $SLURM_ARRAY_TASK_ID \
    --data_path /home/muelleph/automl_project/data \
    --out_path /home/muelleph/automl_project/out_path/BOHB_opt/VarAutoEncoder \
    --no_cuda True

# Print some Information about the end-time to STDOUT
echo "DONE";
echo "Finished at $(date)";
